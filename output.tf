# Cluster Related
output "cluster_id" {
  description = "EKS Cluster ID"
  value       = module.eks.cluster_id
}
output "cluster_security_group_id" {
  description = "SG ID used for the EKS Cluster Control Plane"
  value       = module.eks.cluster_security_group_id
}
output "cluster_endpoint" {
  description = "EKS Cluster Control Plane Endpoint"
  value       = module.eks.cluster_endpoint
}
output "cluster_name" {
  description = "EKS Cluster Name"
  value       = module.eks.cluster_name
}

# Kubectl Related
output "kubectl_config" {
  description = "kubectl config file"
  value       = module.eks.aws_auth_configmap_yaml
}
output "config_map_aws_auth" {
  description = "K8s config map for EKS authentication"
  value       = module.eks.aws_auth_configmap_yaml
}

