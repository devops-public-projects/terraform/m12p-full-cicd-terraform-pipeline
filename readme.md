# Infrastructure as Code with Terraform
Create Replica K8s clusters by using a script instead of manually deploying them

## Technologies Used
- Terraform
- AWS
	- S3
	- EKS
- Jenkins
- Helm
- MySQL
- Git
- Java
- Linux


## Project Description
- Create an EKS Cluster with Terraform
	- 3 Nodes
	- 1 Fargate Profile
- Deploy MySQL with 3 Replicas and Data Persistance
- Configure Remote State with AWS S3 Bucket
- Create a Jenkins CI/CD Pipeline for the Terraform Project

## Prerequisites
- AWS Account
- All Previous Tools Installed
	- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
	- [aws-iam-authenticator](https://github.com/kubernetes-sigs/aws-iam-authenticator)
	- [Terraform Installed](https://developer.hashicorp.com/terraform/install)
	- [kubectl](https://kubernetes.io/docs/tasks/tools/)
-   Jenkins Server
-   Docker Hub Account

## Guide Steps
### Initial Steps
- Create a GitLab repository for project (This will act as our revision control for Terraform)

#### Create Providers.tf
- Create a new file called `providers.tf`
- We will utilize the hashicorp/aws provider, which according to the [EKS Module](https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/latest) currently requires version ">= 5.38" of the AWS provider

### EXERCISE 1: Create Terraform project to spin up EKS cluster
#### EKS Configuration File
- We will use the [EKS Module](https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/latest) provided by AWS. We will put the sample "Usage" into a file called `eks-cluster.tf` and modify it

- File Modifications:
	- **cluster_name**: var.cluster_name
	- **cluster_version**: var.cluster_version
	- **vpc_id**: module.vpc.vpc_id
		- Exposed by EKS > VPC Module
	- **subnet_ids**: module.vpc.private_subnets
		- Exposed by EKS > VPC Module
	- **min_size**: 1
	- **max_size**: 3
	- **desired_size**: 3
	- **instance_type**: 't3.small'

- File Additions:
	- **cluster_addons** > `aws-ebs-csi-driver = {}`
	- **tags**: `Name = "${var.env_prefix}"`
	- **iam_role_additional_policies**: `AmazonEBSCSIDriverPolicy`
		- From documentation on EBS CSI Driver we need to add an additional policy (Seen from IAM UI > Policies)
	- **fargate_profiles** section
		- **name**: "fargate-profile"
			- A generic name for our fargate profile
		- **selectors.namespace**: "my-app"
			- A generic name for our app
- File Removals
	- **control_plane_subnet_ids**
		- We are utilizing AWS Control Plane which is not controlled by us
	- **eks_managed_node_group_defaults**
		- We will use non-default node settings
	- **capacity_type**
	- We'll also remove the entire "Cluster Access Entry" block

#### Variables File Configuration
- We want to set defaults for the values we previously configured as well as declaring them.
	- **env_prefix**: "dev"
	- **k8s_version**: "1.28"
	- **cluster_name**: "myapp-cluster"
	- **region**: "us-east-1"

#### VPC File Configuration
- Create a file `vpc.tf`
	- I will utilize a combination of information from two previous projects to combine [Remote State settings](https://gitlab.com/devops-public-projects/terraform/m12-4-terraform-shared-remote-state/-/blob/main/java-maven-app/terraform/main.tf?ref_type=heads) & [VPC settings](https://gitlab.com/devops-public-projects/terraform/m12-5-full-cicd-terraform-pipeline/-/blob/main/java-maven-app/terraform/main.tf?ref_type=heads)
- File Additions
	- Add the terraform block with backend "s3"
	- Add provider block for "aws"
	- [VPC Module Usage Template](https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/latest)
	- Subnets | My Region "us-east-1" has 6 AZ's and the best practice is 6 Public, 6 Private subnets, one on each AZ
	- We need to tag the VPC, public and private subnets to link correctly to the cluster ([Example Tagging](https://gitlab.com/devops-public-projects/terraform/m12-3-automate-aws-eks/-/blob/main/vpc.tf?ref_type=heads))

#### Example Override tfvars
- Create a file called `dev.tfvars` to showcase how we can override values from the defaults
- I set some values (**env_prefix**, **cluster_name**) to be different than the `variables.tf` default


#### MySQL Configuration File
- Create a `mysql.tf` file and base the usage from the [Terraform Helm Documentation](https://registry.terraform.io/providers/hashicorp/helm/latest/docs)
- We will set the data queries in order to figure out what cluster we are using
- We will add the K8s provider, the Helm Provider and configure the [Terraform resource for MySQL](https://stackoverflow.com/questions/65482908/how-to-deploy-a-bitnami-chart-on-k8s-with-terraform-passing-a-custom-values-yaml)
- [Cluster Authorization Configuration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster_auth)

#### MySQL Values Configuration File
- [Example File Usage](https://gitlab.com/devops-public-projects/kubernetes/m10p-full-k8s-deploy/-/blob/main/kubernetes-exercises/mysql-values.yaml?ref_type=heads)
- The "storageClass" is [gp2 or gp3](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AmazonEBS.html), gp3 is faster but we this is a demo so we'll use **gp2**

#### Output Configuration File
- Create a `output.tf` file
- We will need some outputs for the kubectl configuration later as well as some miscellaneous outputs ([Alot of Sample Outputs](https://github.com/Azure-Terraform/terraform-azurerm-kubernetes/blob/master/output.tf))

### EXERCISE 2: Configure remote state 
#### Create S3 Bucket
- AWS UI > S3 > **Create bucket**
	- **Bucket Name**: "<GLOBALLY_UNIQUE_NAME>
	- **AWS Region**: <YOUR_DEFAULT_REGION>
	- **Bucket Key**: Disable
	- **Create**
- Previous code is already included in `vpc.tf` under the terraform block

### EXERCISE 3: CI/CD pipeline for Terraform project
#### Setup Jenkins to Create EKS Cluster
[Jenkinsfile Provision Sample](https://gitlab.com/devops-public-projects/terraform/m12-5-full-cicd-terraform-pipeline/-/blob/main/java-maven-app/Jenkinsfile?ref_type=heads)
- We only want to provision the EKS and deploy our MySQL Helm Chart so we will only use **1** build stage for the example
- Global Requirements (Credentials stored in Jenkins to access AWS)
	- AWS_ACCESS_KEY_ID
	- AWS_SECRET_ACCESS_KEY
- Build Stage Requirements
	- We overrode 4 variables in `dev.tfvars` we will set them in the "provision cluster" block as well with our **TF_VAR_** prefix so Terraform sees them.
	- To run terraform in a non-interactive way we need the `--auto-approve`  flag for automation to work
	- We also want to use our sample `dev.tfvars` with the argument `-var-file="dev.tfvars"`
	- After Terraform creates our cluster, we would like to output the URL for it into the environment. This will allow us to deploy to it later so we know where.

### Run Build
- The creation time can be anywhere from **20 to 30 minutes**
- I also ran into a problem where us-east-1e AZ did not have capacity to handle my request so I hard-coded workaround values to exclude it and that fixed the problem


#### Pipeline Build Success
![Pipeline Build Success](/images/m12p-build-success.png)

#### Nodes Created
![Nodes Created](/images/m12p-nodes-created.png)


#### Services Running
![Services Running](/images/m12p-services-running.png)