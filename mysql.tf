data "aws_eks_cluster" "cluster" {
  depends_on = [data.aws_eks_cluster.cluster]
  name       = module.eks.cluster_name
}

data "aws_eks_cluster_auth" "cluster-auth" {
  depends_on = [data.aws_eks_cluster.cluster]
  name       = data.aws_eks_cluster.cluster.name
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  token                  = data.aws_eks_cluster_auth.cluster-auth.token
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    token                  = data.aws_eks_cluster_auth.cluster-auth.token
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  }

}

resource "helm_release" "mysql" {
  name       = "my-app-release"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "mysql"
  version    = "9.14.0"
  timeout    = "1000"

  values = [
    "${file("mysql-values.yaml")}"
  ]

  set {
    name  = "volumePermissions.enabled"
    value = true
  }
}
