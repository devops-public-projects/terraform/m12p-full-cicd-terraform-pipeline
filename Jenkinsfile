#!/usr/bin/env groovy

pipeline {
    agent any
    environment {
        AWS_ACCESS_KEY_ID = credentials('jenkins_aws_access_key_id')
        AWS_SECRET_ACCESS_KEY = credentials('jenkins_aws_secret_access_key')
    }
    stages {
        stage('provision cluster') {
            environment {
                TF_VAR_env_prefix   = "development"
                TF_VAR_k8s_version  = "1.28"
                TF_VAR_cluster_name = "myapp-cluster"
                TF_VAR_region       = "us-east-1"
                TF_VAR_values_file   = "dev.tfvars"
            }
            steps {
                script {
                    sh "terraform init"
                    sh "terraform refresh -target=data.aws_eks_cluster_auth.cluster"
                    sh "terraform apply -var-file=${TF_VAR_values_file} --auto-approve"

                    env.K8S_CLUSTER_URL = sh(
                        script: "terraform output cluster_endpoint",
                        returnStdout: true
                    ).trim()

                    sh "aws eks update-kubeconfig --name ${TF_VAR_cluster_name} --region ${TF_VAR_region}"
                }
            }
        }
    }
}