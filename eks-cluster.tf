module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.20.0"

  cluster_name    = var.cluster_name
  cluster_version = var.k8s_version

  cluster_endpoint_public_access = true

  cluster_addons = {
    coredns = {
      most_recent = true
    }
    kube-proxy = {
      most_recent = true
    }
    vpc-cni = {
      most_recent = true
    }
    aws-ebs-csi-driver = {}
  }

  vpc_id     = module.vpc.vpc_id
  subnet_ids = module.vpc.private_subnets

  # EKS Managed Node Group(s)
  eks_managed_node_groups = {
    nodegroup = {
      min_size     = var.min_size
      max_size     = var.max_size
      desired_size = var.desired_size

      instance_types       = var.instance_types
      node_group_name      = var.env_prefix
      use_custom_templates = false

      tags = {
        Name = "${var.env_prefix}"
      }

      # EBS CSI Driver
      iam_role_additional_policies = {
        AmazonEBSCSIDriverPolicy = "arn:aws:iam::aws:policy/service-role/AmazonEBSCSIDriverPolicy"
      }

    }
  }
  fargate_profiles = {
    profile = {
      name = "fargate-profile"
      selectors = [
        {
          namespace = "myapp"
        }
      ]
    }
  }

  tags = {
    environment = var.env_prefix
    terraform   = "true"
  }
}
