# Most are set in variables.tf but this is to showcase how you could override if needed
env_prefix  = "development"
k8s_version = "1.28"
region      = "us-east-1"
