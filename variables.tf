variable "env_prefix" {
  default = "dev"
}
variable "k8s_version" {
  default = "1.28"
}
variable "cluster_name" {
  default = "myapp-cluster"
}
variable "region" {
  default = "us-east-1"
}
variable "instance_types" {
  default = ["t3.small"]
}
variable "min_size" {
  default = 1
}
variable "max_size" {
  default = 3
}
variable "desired_size" {
  default = 3
}
variable "vpc_cidr_block" {
  default = "10.0.0.0/16"
}
variable "public_subnet_cidr_blocks" {
  default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24", "10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
}
variable "private_subnet_cidr_blocks" {
  default = ["10.0.7.0/24", "10.0.8.0/24", "10.0.9.0/24", "10.0.10.0/24", "10.0.11.0/24", "10.0.12.0/24"]
}
